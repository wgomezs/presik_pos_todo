
import os
from PyQt5.QtGui import QIcon

current_dir = os.path.dirname(__file__)


def get_icon(name):
    file_icon = name if name else 'fork'
    path_icon = os.path.join(current_dir, 'share', file_icon + '.svg')
    _icon = QIcon(path_icon)
    return _icon


def to_numeric(number):
    return str(round(number, 2))


def to_float(number, digits):
    return str(round(number, 4))
