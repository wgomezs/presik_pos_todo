import os
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QGridLayout, QHBoxLayout, QStackedWidget

from .commons.custom_button import CustomButton
from .tools import get_icon

DIR_SHARE = os.path.abspath(os.path.normpath(os.path.join(__file__,
    '..', '..', 'share')))

__all__ = ['ButtonsFunction', 'ButtonsStacked', 'ButtonsNumber']


def factoryIcons():
    pass


factoryIcons()


class ButtonsFunction(QGridLayout):
    # Function Numpad

    def __init__(self, parent, tablet_mode=False, panel=False, values=[]):
        super(ButtonsFunction, self).__init__()

        rows = 4
        columns = 3
        self.setHorizontalSpacing(1)
        self.setVerticalSpacing(1)
        self.parent = parent
        self.set_values(values)
        name_style = 'toolbar_button'
        if self.parent.enviroment == 'restaurant' and not panel:
            rows = 4
            columns = 5
        if panel:
            name_style = 'toolbar'

        positions = [(i, j) for i in range(rows) for j in range(columns)]
        for position, value in zip(positions, self.values):
            name_icon = value[0][7:]
            button = CustomButton(
                parent,
                id=value[0],
                icon=get_icon(name_icon),
                title=value[1],
                method=value[2],
                name_style=name_style
            )
            self.addWidget(button, *position)

    def set_values(self, values):
        if values:
            self.values = values
            return
        self.values = [
            ['button_search_product', self.tr('SEARCH'), 'action_search_product'],
        ]

        salesman_desc = self.tr('SALESMAN')
        salesman_button = 'button_salesman'
        if self.parent.enviroment == 'restaurant':
            salesman_desc = self.tr('WAITER')
            salesman_button = 'button_waiter'

        self.values.extend([
            ['button_party', self.tr('CUSTOMER'), 'action_party'],
            ['button_cancel', self.tr('CANCEL'), 'action_cancel'],
            ['button_search_sale', self.tr('S. SALE'), 'action_search_sale'],
            ['button_print_sale', self.tr('PRINT'), 'action_print_sale'],
            [salesman_button, salesman_desc, 'action_salesman'],
            ['button_delivery_men', self.tr('DELIVERY MEN'), 'action_delivery_men']
            # ['button_position', self.tr('POSITION'), 'action_position'],
            # ['button_global_discount', self.tr('GLOBAL DISCOUNT'), 'action_global_discount'],
        ])

        if self.parent.type_pos_user != 'cashier':
            self.values.append(
                ['button_new_sale', self.tr('NEW SALE'), 'action_new_sale'])

        if self.parent.type_pos_user != 'order':
            self.values.append(['button_payment', self.tr('PAY MODE'), 'action_payment'])
            self.values.append([
                'button_payment_term', self.tr('PAY TERM'), 'action_payment_term']
            )

        if self.parent._web_channel:
            self.values.append([
                'button_channel', self.tr('CHANNELS'), 'action_channel']
            )

        if self.parent.enviroment == 'restaurant':
            self.values.extend([
                ['button_comment', self.tr('NOTE'), 'action_comment'],
                ['button_tip', self.tr('TIP'), 'action_tip'],
                ['button_tables', self.tr('TABLES'), 'action_tables'],
                ['button_reservations', self.tr('RESERVATIONS'), 'action_reservations'],
                ['button_consumer', self.tr('CONSUMER'), 'action_consumer'],
                ['button_print_order', self.tr('ORDER'), 'action_print_order']
                # ['button_delete_line', self.tr('DELETE'), 'action_delete_line'],
            ])


class ButtonsStacked(QHBoxLayout):

    def __init__(self, parent):
        super(ButtonsStacked, self).__init__()
        self.stacked = QStackedWidget()

        self.button_accept = CustomButton(
            id='button_accept',
            parent=parent,
            icon=get_icon('accept'),
            title=self.tr('FINISH'),
            name_style='toolbar',
            method='button_accept_pressed'
        )
        self.button_cash = CustomButton(
            id='button_cash',
            parent=parent,
            icon=get_icon('cash'),
            title=self.tr('PAY'),
            name_style='toolbar',
            method='button_cash_pressed'
        )

        if parent.type_pos_user != 'order':
            self.stacked.addWidget(self.button_accept)
            self.stacked.addWidget(self.button_cash)
            self.addWidget(self.stacked, 0)

        if parent.type_pos_user == 'cashier':
            self.button_to_draft = CustomButton(
                id='button_to_draft',
                parent=parent,
                title=self.tr('RETURN TO DRAFT'),
                icon=get_icon('draft'),
                name_style='toolbar',
                method='button_to_draft_pressed'
            )
            self.addWidget(self.button_to_draft, 0)

            # self.button_shipment = CustomButton(
            #     id='button_shipment',
            #     parent=parent,
            #     title=self.tr('GENERATE SHIPMENT'),
            #     icon=get_icon('shipment'),
            #     name_style='toolbar',
            #     method='button_shipment_pressed'
            # )
            # self.addWidget(self.button_shipment, 0)
        # else:
        #     self.button_plus = CustomButton(
        #         id='button_plus',
        #         parent=parent,
        #         icon=get_icon('plus'),
        #         method='button_plus_pressed',
        #         name_style='toolbar',
        #     )
            #self.addWidget(self.button_plus, 0)

        if parent.type_pos_user == 'order' or parent.type_pos_user == 'salesman':
            self.button_send_order = CustomButton(
                id='button_send_to_pay',
                icon=get_icon('draft'),
                parent=parent,
                title=self.tr('GO TO PAY'),
                method='button_send_to_pay_pressed',
                name_style='toolbar'
            )
            self.addWidget(self.button_send_order, 0)


class ButtonsNumber(QGridLayout):

    def __init__(self, parent):
        # Numpad for Numbers
        super(ButtonsNumber, self).__init__()
        self.setHorizontalSpacing(1)
        self.setVerticalSpacing(1)


class Buttonpad(QWidget):

    def __init__(self, parent):
        super(Buttonpad, self).__init__()
        self._text = ''
        self._keyStates = {}
        self.functions = ButtonsFunction(parent)
        if parent.tablet_mode:
            self.numbers = ButtonsNumber(parent)

        self.stacked = ButtonsStacked(parent)
        self.set_keys()

    def set_keys(self):
        q = Qt
        self.keys_numbers = list(range(q.Key_0, q.Key_9 + 1))
        self.keys_alpha = list(range(q.Key_A, q.Key_Z + 1))
        self.keys_special = [
            q.Key_Asterisk, q.Key_Comma, q.Key_Period,
            q.Key_Minus, q.Key_Slash]
        self.show_keys = self.keys_numbers + self.keys_alpha + self.keys_special
