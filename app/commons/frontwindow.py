# -*- coding: UTF-8 -*-
import os
import time
import logging
from pathlib import Path

from PyQt5.QtWidgets import QMainWindow, QDesktopWidget, QLabel
from PyQt5.QtCore import QTimer, QThread, pyqtSignal, Qt

from .dialogs import QuickDialog
from .dblogin import safe_reconnect


__all__ = ['FrontWindow', 'ClearUi']

parent = Path(__file__).parent.parent

file_base_css = os.path.join(str(parent), 'css', 'base.css')
_DEFAULT_TIMEOUT = 60000  # on ms (100 minutes)
path_trans = os.path.join(os.path.abspath(
    os.path.dirname(__file__)), 'locale', 'i18n_es.qm')


class FrontWindow(QMainWindow):

    def __init__(self, connection, params, title=None, show_mode=None):
        super(FrontWindow, self).__init__()
        if not title:
            title = self.tr('APPLICATION')

        self._state = None
        self._keyStates = {}
        self.window().setWindowTitle(title)
        self.setObjectName('WinMain')
        self.conn = connection
        self._context = connection.context
        self.set_params(params)
        self.logger = logging.getLogger('neox_logger')

        """
        We need get the size of screen (display)
            ---------------  -------------------
                  name           width (px)
            ---------------  -------------------
            small screen     =< 1024
            medium screen    > 1024 and =< 1366
            large screen     > 1366
        """

        screen = QDesktopWidget().screenGeometry()
        self.setGeometry(0, 0, screen.width(), screen.height())
        screen_width = screen.width()
        print('Screen width : ', screen_width)

        self.screen_size = 'large'
        if screen_width <= 1024:
            self.screen_size = 'small'
        elif screen_width <= 1366:
            self.screen_size = 'medium'

        self.timeout = _DEFAULT_TIMEOUT
        self.set_stack_messages()

        if show_mode == 'fullscreen':
            self.window().showFullScreen()
        else:
            self.window().show()
        self.setFocus()
        self.global_timer = 0

    def set_stack_messages(self):
        self.stack_msg = {}

    def get_geometry(self):
        screen = QDesktopWidget().screenGeometry()
        return screen.width(), screen.height()

    def set_statusbar(self, values):
        status_bar = self.statusBar()
        status_bar.setSizeGripEnabled(False)

        for k, v in values.items():
            _label = QLabel(v['name'] + ':')
            _label.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
            status_bar.addWidget(_label, 1)
            setattr(self, k, QLabel(str(v['value'])))
            _label_info = getattr(self, k)
            _label_info.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
            status_bar.addWidget(_label_info)

    def set_style(self, file_css, theme_css=None):
        styles = []
        if theme_css:
            theme_css = os.path.join(str(parent), 'css', theme_css + '.css')
        for style in [theme_css or file_base_css, file_css]:
            with open(style, 'r') as infile:
                styles.append(infile.read())
        self.setStyleSheet(''.join(styles))

    def set_timeout(self):
        if self.active_timeout != 'True':
            return

        self.timeout = eval(self.timeout)
        if not self.timeout:
            self.timeout = _DEFAULT_TIMEOUT
        timer = QTimer(self)
        timer.timeout.connect(self.count_time)
        timer.start(1000)

    def count_time(self):
        self.global_timer += 1
        if self.global_timer > self.timeout:
            self.global_timer = 0
            safe_reconnect()

    def dialog(self, name, response=False):
        res = QuickDialog(
            parent=self,
            kind=self.stack_msg[name][0],
            string=self.stack_msg[name][1],
        )
        return res

    def set_params(self, values):
        for k, v in values.items():
            if v in ('False', 'True'):
                v = eval(v)
            setattr(self, k, v)

    def action_block(self):
        safe_reconnect(self)

    def dialog_password_accept(self):
        self.connection()

    def dialog_password_rejected(self):
        self.connection()

    def keyReleaseEvent(self, event):
        self._keyStates[event.key()] = False


class ClearUi(QThread):
    sigActionClear = pyqtSignal()
    state = None

    def __init__(self, wait_time):
        QThread.__init__(self)
        self.wait_time = wait_time

    def run(self):
        time.sleep(self.wait_time)
        self.sigActionClear.emit()
