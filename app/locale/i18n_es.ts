<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_CO">
<context>
    <name>ActionButton</name>
    <message>
        <location filename="../commons/buttons.py" line="12"/>
        <source>&amp;ACCEPT</source>
        <translation>&amp;ACEPTAR</translation>
    </message>
    <message>
        <location filename="../commons/buttons.py" line="14"/>
        <source>&amp;CANCEL</source>
        <translation>&amp;CANCELAR</translation>
    </message>
</context>
<context>
    <name>ButtonsFunction</name>
    <message>
        <location filename="../buttonpad.py" line="58"/>
        <source>SEARCH</source>
        <translation>BUSCAR</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="67"/>
        <source>CUSTOMER</source>
        <translation>CLIENTE</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="67"/>
        <source>CANCEL</source>
        <translation>CANCELAR</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="67"/>
        <source>PRINT</source>
        <translation>IMPRIMIR</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="61"/>
        <source>SALESMAN</source>
        <translation>VENDEDOR</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="94"/>
        <source>ORDER</source>
        <translation>ENV. ORDEN</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="79"/>
        <source>NEW SALE</source>
        <translation>NUEVA VENTA</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="83"/>
        <source>PAY MODE</source>
        <translation>MEDIO DE PAGO</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="84"/>
        <source>PAY TERM</source>
        <translation>PLAZO DE PAGO</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="94"/>
        <source>NOTE</source>
        <translation>NOTA</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="94"/>
        <source>TIP</source>
        <translation>PROPINA</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="94"/>
        <source>TABLES</source>
        <translation>MESAS</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="94"/>
        <source>RESERVATIONS</source>
        <translation>RESERVACIONES</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="67"/>
        <source>S. SALE</source>
        <translation>B. VENTA</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="64"/>
        <source>WAITER</source>
        <translation>MESERO</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="94"/>
        <source>CONSUMER</source>
        <translation>CONSUMIDOR</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="67"/>
        <source>DELIVERY MEN</source>
        <translation>DOMICILIARIOS</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="89"/>
        <source>CHANNELS</source>
        <translation>CANALES</translation>
    </message>
</context>
<context>
    <name>ButtonsStacked</name>
    <message>
        <location filename="../buttonpad.py" line="111"/>
        <source>FINISH</source>
        <translation>FINALIZAR</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="119"/>
        <source>PAY</source>
        <translation>PAGAR</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="134"/>
        <source>RETURN TO DRAFT</source>
        <translation>DEVOLVER A BORRADOR</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="164"/>
        <source>GO TO PAY</source>
        <translation>IR A PAGAR</translation>
    </message>
    <message>
        <location filename="../buttonpad.py" line="144"/>
        <source>GENERATE SHIPMENT</source>
        <translation>GENERAR ENVIO</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <location filename="../dialogs.py" line="848"/>
        <source>COMMENTS</source>
        <translation>COMENTARIOS</translation>
    </message>
</context>
<context>
    <name>ControlPanel</name>
    <message>
        <location filename="../dialogs.py" line="52"/>
        <source>CONTROL PANEL</source>
        <translation>PANEL DE CONTROL</translation>
    </message>
</context>
<context>
    <name>DeliveryPartySelected</name>
    <message>
        <location filename="../dialogs.py" line="607"/>
        <source>DELIVERY MEN:</source>
        <translation>DOMICILIARIO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="617"/>
        <source>ID NUMBER:</source>
        <translation>NÚMERO ID</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="627"/>
        <source>NUMBER PLATE:</source>
        <translation>NÚMERO DE PLACA</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="647"/>
        <source>TYPE VEHICLE:</source>
        <translation>TIPO DE VEHÍCULO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="658"/>
        <source>ACTIVE:</source>
        <translation>ACTIVO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="637"/>
        <source>PHONE:</source>
        <translation>TELEFONO</translation>
    </message>
</context>
<context>
    <name>DialogAgent</name>
    <message>
        <location filename="../dialogs.py" line="418"/>
        <source>AGENT</source>
        <translation>AGENTE</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="418"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="418"/>
        <source>NAME</source>
        <translation>NOMBRE</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="418"/>
        <source>ID NUMBER</source>
        <translation>NUMERO ID</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="428"/>
        <source>COMMISSION</source>
        <translation>COMISIÓN</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="429"/>
        <source>AMOUNT</source>
        <translation>VALOR</translation>
    </message>
</context>
<context>
    <name>DialogCancelInvoice</name>
    <message>
        <location filename="../dialogs.py" line="441"/>
        <source>INSERT PASSWORD FOR CANCEL</source>
        <translation>INGRESE LA CONTRASEÑA PARA CANCELAR</translation>
    </message>
</context>
<context>
    <name>DialogChannel</name>
    <message>
        <location filename="../dialogs.py" line="789"/>
        <source>SELECT CHANNEL</source>
        <translation>SELECCIONAR CANAL</translation>
    </message>
</context>
<context>
    <name>DialogConsumer</name>
    <message>
        <location filename="../dialogs.py" line="206"/>
        <source>PHONE:</source>
        <translation>TELEFONO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="215"/>
        <source>CONSUMER:</source>
        <translation>CONSUMIDOR</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="224"/>
        <source>ADDRESS:</source>
        <translation>DIRECCIÓN</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="233"/>
        <source>ID NUMBER:</source>
        <translation>NÚMERO ID</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="242"/>
        <source>BIRTHDAY:</source>
        <translation>CUMPLEAÑOS</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="255"/>
        <source>NOTES:</source>
        <translation>NOTAS</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="275"/>
        <source>SALES HISTORY</source>
        <translation>HISTORIAL DE VENTAS</translation>
    </message>
</context>
<context>
    <name>DialogDeliveryMen</name>
    <message>
        <location filename="../dialogs.py" line="582"/>
        <source>CHOOSE DELIVERY MEN</source>
        <translation>SELECCIONE DOMICILIARIO</translation>
    </message>
</context>
<context>
    <name>DialogForceAssign</name>
    <message>
        <location filename="../dialogs.py" line="456"/>
        <source>PASSWORD FORCE ASSIGN</source>
        <translation>CONTRASEÑA PARA FORZAR ASIGNACIÓN</translation>
    </message>
</context>
<context>
    <name>DialogGlobalDiscount</name>
    <message>
        <location filename="../dialogs.py" line="492"/>
        <source>GLOBAL DISCOUNT</source>
        <translation>DESCUENTO GLOBAL</translation>
    </message>
</context>
<context>
    <name>DialogGlobalDiscountTable</name>
    <message>
        <location filename="../dialogs.py" line="564"/>
        <source>CHOOSE DISCOUNT</source>
        <translation>SELECCIONE DESCUENTO</translation>
    </message>
</context>
<context>
    <name>DialogOrder</name>
    <message>
        <location filename="../dialogs.py" line="466"/>
        <source>DO YOU WANT TO CONFIRM THE SEND ORDER?</source>
        <translation>DESEAS CONFIRMAR EL ENVIO DE LA ORDEN?</translation>
    </message>
</context>
<context>
    <name>DialogPayment</name>
    <message>
        <location filename="../dialogs.py" line="820"/>
        <source>SELECT PAYMENT MODE:</source>
        <translation>SELECCIONE EL MEDIO DE PAGO:</translation>
    </message>
</context>
<context>
    <name>DialogPaymentTerm</name>
    <message>
        <location filename="../dialogs.py" line="804"/>
        <source>SELECT PAYMENT TERM</source>
        <translation>SELECCIONE EL MODO DE PAGO</translation>
    </message>
</context>
<context>
    <name>DialogPrintInvoice</name>
    <message>
        <location filename="../dialogs.py" line="503"/>
        <source>INVOICE NUMBER</source>
        <translation>NUMERO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="504"/>
        <source>PRINTER</source>
        <translation>IMPRESORA</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="512"/>
        <source>TYPE</source>
        <translation>TIPO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="512"/>
        <source>INVOICE</source>
        <translation>FACTURA</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="512"/>
        <source>ORDER</source>
        <translation>PEDIDO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="512"/>
        <source>DELIVERY</source>
        <translation>DOMICILIO</translation>
    </message>
</context>
<context>
    <name>DialogSalesman</name>
    <message>
        <location filename="../dialogs.py" line="542"/>
        <source>CHOOSE SALESMAN</source>
        <translation>ESCOGE EL VENDEDOR</translation>
    </message>
</context>
<context>
    <name>DialogStock</name>
    <message>
        <location filename="../dialogs.py" line="479"/>
        <source>WAREHOUSE</source>
        <translation>BODEGA</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="479"/>
        <source>QUANTITY</source>
        <translation>CANTIDAD</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="481"/>
        <source>STOCK BY PRODUCT:</source>
        <translation>INVENTARIO POR PRODUCTO:</translation>
    </message>
</context>
<context>
    <name>DialogTableDeliveryParty</name>
    <message>
        <location filename="../dialogs.py" line="680"/>
        <source>DELIVERY MAN</source>
        <translation>DOMICILIARIOS</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="704"/>
        <source>CREATE NEW DELIVERY MEN</source>
        <translation>CREAR NUEVO DOMICILIARIO</translation>
    </message>
</context>
<context>
    <name>DialogTableMoneyCount</name>
    <message>
        <location filename="../dialogs.py" line="721"/>
        <source>MONEY COUNT</source>
        <translation>CONTAR DINERO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="753"/>
        <source>TOTAL MONEY:</source>
        <translation>TOTAL DINERO</translation>
    </message>
</context>
<context>
    <name>DialogTableSaleConsumer</name>
    <message>
        <location filename="../dialogs.py" line="293"/>
        <source>SALES</source>
        <translation>VENTAS</translation>
    </message>
</context>
<context>
    <name>DialogTaxes</name>
    <message>
        <location filename="../dialogs.py" line="777"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="777"/>
        <source>Salesman</source>
        <translation>Vendedor</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="779"/>
        <source>CHOOSE TAX</source>
        <translation>ESCOJA EL IMPUESTO</translation>
    </message>
</context>
<context>
    <name>DialogVoucher</name>
    <message>
        <location filename="../dialogs.py" line="532"/>
        <source>VOUCHER NUMBER</source>
        <translation>NÚMERO DE VOUCHER</translation>
    </message>
</context>
<context>
    <name>FrontWindow</name>
    <message>
        <location filename="../commons/frontwindow.py" line="29"/>
        <source>APPLICATION</source>
        <translation>APLICACIÓN</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../dialogs.py" line="930"/>
        <source>SEARCH PRODUCT</source>
        <translation>BUSCAR PRODUCTO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="931"/>
        <source>PAYMENT MODE</source>
        <translation>MODO DE PAGO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="932"/>
        <source>SEARCH CUSTOMER</source>
        <translation>BUSCAR CLIENTE</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="933"/>
        <source>GLOBAL DISCOUNT</source>
        <translation>DESCUENTO GLOBAL</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="934"/>
        <source>PRINT ORDER</source>
        <translation>IMPRIMIR ORDEN</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="935"/>
        <source>PRINT INVOICE</source>
        <translation>IMPRIMIR FACTURA</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="936"/>
        <source>PAYMENT TERM</source>
        <translation>PLAZO DE PAGO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="937"/>
        <source>SEARCH SALES</source>
        <translation>BUSCAR VENTAS</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="938"/>
        <source>ACTIVATE TABLE</source>
        <translation>ACTIVAR TABLA</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="939"/>
        <source>NEW SALE</source>
        <translation>NUEVA VENTA</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="941"/>
        <source>SALESMAN</source>
        <translation>VENDEDOR</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="942"/>
        <source>POSITION</source>
        <translation>POSICIÓN</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="943"/>
        <source>CASH</source>
        <translation>PAGO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="944"/>
        <source>COMMENT</source>
        <translation>COMENTARIO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="929"/>
        <source>PANEL</source>
        <translation>PANEL</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="940"/>
        <source>CONTROL PANEL</source>
        <translation>PANEL DE CONTROL</translation>
    </message>
</context>
<context>
    <name>HelpDialog</name>
    <message>
        <location filename="../commons/dialogs.py" line="312"/>
        <source>Keys Shortcuts...</source>
        <translation>ATAJOS DE TECLADO</translation>
    </message>
    <message>
        <location filename="../commons/dialogs.py" line="323"/>
        <source>Action</source>
        <translation>ACCIÓN</translation>
    </message>
    <message>
        <location filename="../commons/dialogs.py" line="324"/>
        <source>Shortcut</source>
        <translation>ATAJOS</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../commons/dblogin.py" line="69"/>
        <source>HOST</source>
        <translation>SERVIDOR</translation>
    </message>
    <message>
        <location filename="../commons/dblogin.py" line="69"/>
        <source>DATABASE</source>
        <translation>BASE DE DATOS</translation>
    </message>
    <message>
        <location filename="../commons/dblogin.py" line="69"/>
        <source>USER</source>
        <translation>USUARIO</translation>
    </message>
    <message>
        <location filename="../commons/dblogin.py" line="69"/>
        <source>PASSWORD</source>
        <translation>CONTRASEÑA</translation>
    </message>
    <message>
        <location filename="../commons/dblogin.py" line="80"/>
        <source>C&amp;ANCEL</source>
        <translation>CANCELAR</translation>
    </message>
    <message>
        <location filename="../commons/dblogin.py" line="83"/>
        <source>&amp;CONNECT</source>
        <translation>&amp;CONECTAR</translation>
    </message>
    <message>
        <location filename="../commons/dblogin.py" line="99"/>
        <source>Error: username or password invalid...!</source>
        <translation>Error: nombre de usuario o contraseña invalida</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>SYSTEM READY...</source>
        <translation>SISTEMA LISTO...</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>DO YOU WANT TO EXIT?</source>
        <translation>DESEA SALIR?</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>PLEASE CONFIRM YOUR PAYMENT TERM AS CREDIT?</source>
        <translation>POR FAVOR CONFIRMAR SI SU PLAZO DE PAGO ES CREDITO?</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>SALE ORDER / INVOICE NUMBER NOT FOUND!</source>
        <translation>ORDER / FACTURA DE VENTA NO ENCONTRADA!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>THIS SALE IS CLOSED, YOU CAN NOT TO MODIFY!</source>
        <translation>ESTA VENTA ESTA CERRADA, Y USTED NO PUEDE MODIFICARLA!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>DISCOUNT VALUE IS NOT VALID!</source>
        <translation>EL DESCUENTO NO ES VALIDO!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>YOU CAN NOT ADD PAYMENTS TO SALE ON DRAFT STATE!</source>
        <translation>NO PUEDE AGREGAR PAGOS A UNA VENTA EN BORRADOR!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>ENTER QUANTITY...</source>
        <translation>INGRESE LA CANTIDAD...</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>ENTER DISCOUNT...</source>
        <translation>INGRESE EL DESCUENTO...</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>ENTER PAYMENT AMOUNT BY: %s</source>
        <translation>INGRESE EL VALOR DEL PAGO EN: %s</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>ENTER NEW PRICE...</source>
        <translation>INGRESE EL NUEVO PRECIO...</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>ORDER SUCCESUFULLY SENT.</source>
        <translation>ORDEN ENVIADA EXITOSAMENTE.</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>FAILED SEND ORDER!</source>
        <translation>FALLO EL ENVIO DE LA ORDEN!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>MISSING AGENT!</source>
        <translation>FALTA EL AGENTE!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>THERE IS NOT SALESMAN FOR THE SALE!</source>
        <translation>NO SE DEFINIDO EL VENDEDOR EN LA VENTA!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>YOU CAN NOT CONFIRM A SALE WITHOUT PRODUCTS!</source>
        <translation>NO PUEDE CONFIRMAR UNA VENTA SIN PRODUCTOS!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>USER WITHOUT PERMISSION FOR SALE POS!</source>
        <translation>USUARIO SIN PERMISOS PARA VENTA POS!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>THE QUANTITY IS NOT VALID...!</source>
        <translation>LA CANTIDAD NO ES VALIDAD...!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>MISSING THE DEFAULT PARTY ON SHOP CONFIGURATION!</source>
        <translation>FALTA CONFIGURAR EL TERCERO EN LA TIENDA!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>MISSING SET THE JOURNAL ON DEVICE!</source>
        <translation>FALTA EL ESTADO DE CUENTA PARA LA CAJA!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>PRODUCT NOT FOUND!</source>
        <translation>PRODUCTO NO ENCONTRADO!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>DO YOU WANT CREATE NEW SALE?</source>
        <translation>DESEA CREAR UNA NUEVA VENTA?</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>ARE YOU WANT TO CANCEL SALE?</source>
        <translation>DESEA CANCELAR LA VENTA?</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>AGENT NOT FOUND!</source>
        <translation>AGENTE NO ENCONTRADO!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>COMMISSION NOT VALID!</source>
        <translation>LA COMISIÓN NO ES VÁLIDA!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>CREDIT LIMIT FOR CUSTOMER EXCEED!</source>
        <translation>EL CLIENTE SUPERA SU CUPO DE CREDITO!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>THE CUSTOMER CREDIT CAPACITY IS ABOVE 80%</source>
        <translation>EL CUPO DE CREDITO DEL CLIENTE ESTA SOBRE EL 80%</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>YOU CAN NOT FORCE ASSIGN!</source>
        <translation>NO PUEDE FORZAR UNA ASIGACIÓN!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="795"/>
        <source>INVOICE</source>
        <translation>FACTURA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2374"/>
        <source>DATE</source>
        <translation>FECHA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="606"/>
        <source>SALESMAN</source>
        <translation>VENDEDOR</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="620"/>
        <source>PAYMENT TERM</source>
        <translation>PLAZO DE PAGO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="613"/>
        <source>No ORDER</source>
        <translation>No PEDIDO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="653"/>
        <source>POSITION</source>
        <translation>POSICION</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="662"/>
        <source>AGENT</source>
        <translation>AGENTE</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="673"/>
        <source>DELIVERY CHARGE</source>
        <translation>CARGO DOMICILIO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2300"/>
        <source>SUBTOTAL</source>
        <translation>SUBTOTAL</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="740"/>
        <source>TAXES</source>
        <translation>IMPUESTOS</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="747"/>
        <source>DISCOUNT</source>
        <translation>DESCUENTO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="754"/>
        <source>TOTAL</source>
        <translation>TOTAL</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="761"/>
        <source>PAID</source>
        <translation>PAGADO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="775"/>
        <source>CHANGE</source>
        <translation>CAMBIO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="839"/>
        <source>SHOP</source>
        <translation>TIENDA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="839"/>
        <source>DEVICE</source>
        <translation>CAJA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="839"/>
        <source>DATABASE</source>
        <translation>BD</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="839"/>
        <source>USER</source>
        <translation>USUARIO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="839"/>
        <source>PRINTER</source>
        <translation>IMPRESORA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2266"/>
        <source>NAME</source>
        <translation>NOMBRE</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2272"/>
        <source>DESCRIPTION</source>
        <translation>DESCRIPCIÓN</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2434"/>
        <source>AMOUNT</source>
        <translation>VALOR</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2260"/>
        <source>COD</source>
        <translation>COD</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2278"/>
        <source>UNIT</source>
        <translation>UND</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2285"/>
        <source>QTY</source>
        <translation>CANT</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2293"/>
        <source>DISC</source>
        <translation>DESC</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2306"/>
        <source>NOTE</source>
        <translation>NOTA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2314"/>
        <source>UNIT PRICE W TAX</source>
        <translation>PRECIO UNIT CON IMP</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2429"/>
        <source>STATEMENT JOURNAL</source>
        <translation>ESTADO DE CUENTA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>THE USER HAVE NOT PERMISSIONS FOR ACCESS TO DEVICE!</source>
        <translation>EL USUARIO NO TIENE PERMISOS PARA ACCEDER A CAJA!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>THERE IS NOT A STATEMENT OPEN FOR THIS DEVICE!</source>
        <translation>NO HAY ESTADO DE CUENTA ABIERTOS POR ESTA CAJA!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>YOU HAVE NOT PERMISSIONS FOR DELETE THIS SALE!</source>
        <translation>NO TIENE PERMISOS PARA BORRAR ESTA VENTA!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>YOU HAVE NOT PERMISSIONS FOR CANCEL THIS SALE!</source>
        <translation>NO TIENE PERMISOS PARA CANCELAR LA VENTA!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>THE CUSTOMER HAS NOT CREDIT!</source>
        <translation>EL CLIENTE NO TIENE CREDITO!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="673"/>
        <source>CUSTOMER</source>
        <translation>CLIENTE</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="673"/>
        <source>COMPANY</source>
        <translation>COMPAÑIA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="714"/>
        <source>ASSIGNED TABLE</source>
        <translation>MESA ASIGNADA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>FIRST YOU MUST CREATE/LOAD A SALE!</source>
        <translation>PRIMERO DEBE AGREGAR/CARGAR UNA VENTA!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2322"/>
        <source>FRAC</source>
        <translation>FRAC</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>FALLO EL ENVIO DE FACTURA!</source>
        <translation>FALLO EL ENVIO DE FACTURA!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>FALLO FINALIZACIÃN DE FACTURA!</source>
        <translation>FALLO FINALIZACIÓN DE FACTURA°</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="802"/>
        <source>STATE</source>
        <translation>ESTADO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="768"/>
        <source>PENDIENTE</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2438"/>
        <source>VOUCHER</source>
        <translation>COMPROBANTE</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>PRODUCT WITHOUT STOCK: %s</source>
        <translation>PRODUCTO SIN STOCK: %s</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>NOT SALE!...</source>
        <translation>NO HAY VENTA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="549"/>
        <source> TYPE INVOICE:</source>
        <translation>TIPO DE FACTURA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="1776"/>
        <source>DELIVERY MEN</source>
        <translation>DOMICILIARIO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="704"/>
        <source>DELIVERY WAY</source>
        <translation>FORMA DE ENTREGA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>STATEMENTS CREATED!</source>
        <translation>ESTADOS DE CUENTA CREADOS</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>STATEMENTS CLOSED!</source>
        <translation>ESTADOS DE CUENTA CERRADOS</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="643"/>
        <source>CHANNEL</source>
        <translation>CANAL</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="1772"/>
        <source>OPEN STATEMENTS</source>
        <translation>ABRIR ESTADOS DE CUENTA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="1773"/>
        <source>CLOSED STATEMENTS</source>
        <translation>CERRAR ESTADOS DE CUENTA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="1775"/>
        <source>GLOBAL DISCOUNT</source>
        <translation>DESCUENTO GLOBAL</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="1777"/>
        <source>HELP</source>
        <translation>AYUDA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2356"/>
        <source>NUMBER</source>
        <translation>NÚMERO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2362"/>
        <source>INVOICE NUMBER</source>
        <translation>NUMERO DE FACTURA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2368"/>
        <source>CONSUMER</source>
        <translation>CONSUMIDOR</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2380"/>
        <source>TOTAL AMOUNT</source>
        <translation>VALOR TOTAL</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2392"/>
        <source>PARTY</source>
        <translation>TERCERO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2410"/>
        <source>NUMBER PLATE</source>
        <translation>NÚMERO DE PLACA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2416"/>
        <source>TYPE VEHICLE</source>
        <translation>TIPO DE VEHICULO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="700"/>
        <source>TAKE AWAY</source>
        <translation>RECOGER EN EL LUGAR</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="701"/>
        <source>DELIVERY</source>
        <translation>DOMICILIO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="702"/>
        <source>TABLE</source>
        <translation>EN LA MESA</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2330"/>
        <source>COMMANDED</source>
        <translation>COMANDADO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2398"/>
        <source>ID NUMBER</source>
        <translation>NUMERO ID</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="2404"/>
        <source>PHONE</source>
        <translation>TELÉFONO</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>ORDER DISPATCHED!</source>
        <translation>ORDEN DESPACHADA EXITOSAMENTE!</translation>
    </message>
    <message>
        <location filename="../mainwindow.py" line="209"/>
        <source>ERROR TO DISPATCHED ORDER!</source>
        <translation>ERROR AL DESPACHAR ORDEN</translation>
    </message>
</context>
<context>
    <name>MenuWindow</name>
    <message>
        <location filename="../commons/menu_list.py" line="111"/>
        <source>Menu...</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../commons/menu_list.py" line="170"/>
        <source>&amp;ACCEPT</source>
        <translation>&amp;ACEPTAR</translation>
    </message>
    <message>
        <location filename="../commons/menu_list.py" line="173"/>
        <source>&amp;BACK</source>
        <translation>&amp;REGRESAR</translation>
    </message>
</context>
<context>
    <name>Position</name>
    <message>
        <location filename="../dialogs.py" line="837"/>
        <source>POSITION</source>
        <translation>POSICIÓN</translation>
    </message>
</context>
<context>
    <name>QuickDialog</name>
    <message>
        <location filename="../commons/dialogs.py" line="34"/>
        <source>Warning...</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location filename="../commons/dialogs.py" line="35"/>
        <source>Information...</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../commons/dialogs.py" line="36"/>
        <source>Action...</source>
        <translation>Acción</translation>
    </message>
    <message>
        <location filename="../commons/dialogs.py" line="37"/>
        <source>Help...</source>
        <translation>Ayuda...</translation>
    </message>
    <message>
        <location filename="../commons/dialogs.py" line="38"/>
        <source>Error...</source>
        <translation>Error...</translation>
    </message>
    <message>
        <location filename="../commons/dialogs.py" line="39"/>
        <source>Question...</source>
        <translation>Pregunta...</translation>
    </message>
    <message>
        <location filename="../commons/dialogs.py" line="40"/>
        <source>Selection...</source>
        <translation>Selección...</translation>
    </message>
    <message>
        <location filename="../commons/dialogs.py" line="41"/>
        <source>Dialog...</source>
        <translation>Dialogo...</translation>
    </message>
</context>
<context>
    <name>SaleConsumerSelected</name>
    <message>
        <location filename="../dialogs.py" line="326"/>
        <source>PARTY:</source>
        <translation>CLIENTE</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="334"/>
        <source>NUMBER:</source>
        <translation>NÚMERO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="342"/>
        <source>DATE:</source>
        <translation>FECHA</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="350"/>
        <source>SALESMAN:</source>
        <translation>VENDEDOR</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="358"/>
        <source>LINES:</source>
        <translation>LINEAS</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="367"/>
        <source>UNTAXED AMOUNT:</source>
        <translation>BASE</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="375"/>
        <source>TAX AMOUNT:</source>
        <translation>IMPUESTOS</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="383"/>
        <source>TOTAL AMOUNT:</source>
        <translation>TOTAL</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="400"/>
        <source>DUPLICATE SALE</source>
        <translation>DUPLICAR VENTA</translation>
    </message>
</context>
<context>
    <name>SaleLine</name>
    <message>
        <location filename="../dialogs.py" line="875"/>
        <source>FRACTION:</source>
        <translation>FRACCIÓN:</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="885"/>
        <source>QUANTITY:</source>
        <translation>CANTIDAD:</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="902"/>
        <source>UNIT PRICE:</source>
        <translation>PRECIO UNITARIO:</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../commons/dialogs.py" line="241"/>
        <source>Search Products...</source>
        <translation>Buscar Productos</translation>
    </message>
</context>
<context>
    <name>SearchParty</name>
    <message>
        <location filename="../dialogs.py" line="101"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="102"/>
        <source>ID NUMBER</source>
        <translation>NUMERO ID</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="103"/>
        <source>NAME</source>
        <translation>NOMBRE</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="104"/>
        <source>ADDRESS</source>
        <translation>DIRECCION</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="105"/>
        <source>PHONE</source>
        <translation>TELÉFONO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="107"/>
        <source>SEARCH CUSTOMER</source>
        <translation>BUSCAR CLIENTE</translation>
    </message>
</context>
<context>
    <name>SearchProduct</name>
    <message>
        <location filename="../dialogs.py" line="125"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="126"/>
        <source>CODE</source>
        <translation>CÓDIGO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="128"/>
        <source>STOCK</source>
        <translation>INVENTARIO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="137"/>
        <source>NAME</source>
        <translation>NOMBRE</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="142"/>
        <source>DESCRIPTION</source>
        <translation>DESCRIPCIÓN</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="146"/>
        <source>BRAND</source>
        <translation>MARCA</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="149"/>
        <source>PRICE</source>
        <translation>PRECIO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="159"/>
        <source>LOCATION</source>
        <translation>LOCACIÓN</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="163"/>
        <source>IMAGE</source>
        <translation>IMAGEN</translation>
    </message>
</context>
<context>
    <name>SearchSale</name>
    <message>
        <location filename="../dialogs.py" line="74"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="75"/>
        <source>NUMBER</source>
        <translation>NÚMERO</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="76"/>
        <source>INVOICE</source>
        <translation>FACTURA</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="77"/>
        <source>PARTY</source>
        <translation>CLIENTE</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="78"/>
        <source>DATE</source>
        <translation>FECHA</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="79"/>
        <source>SALESMAN</source>
        <translation>VENDEDOR</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="80"/>
        <source>POSITION</source>
        <translation>POSICIÓN</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="81"/>
        <source>TOTAL AMOUNT</source>
        <translation>VALOR TOTAL</translation>
    </message>
    <message>
        <location filename="../dialogs.py" line="84"/>
        <source>SEARCH SALES...</source>
        <translation>BUSCAR VENTAS...</translation>
    </message>
</context>
<context>
    <name>SearchWindow</name>
    <message>
        <location filename="../commons/search_window.py" line="64"/>
        <source>SEARCH...</source>
        <translation>BUSCAR...</translation>
    </message>
    <message>
        <location filename="../commons/search_window.py" line="170"/>
        <source>FILTER:</source>
        <translation>FILTRO:</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../commons/dblogin.py" line="196"/>
        <source>Enter your password:</source>
        <translation>INGRESA TU CONTRASEÑA</translation>
    </message>
</context>
</TS>
